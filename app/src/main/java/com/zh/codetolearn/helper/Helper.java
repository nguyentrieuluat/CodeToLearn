package com.zh.codetolearn.helper;

import android.graphics.Bitmap;

import java.util.HashMap;

/**
 * Created by Strongboss on 16/12/2017.
 */

public class Helper {
    private static Helper instance;
    public static Helper getInstance() {
        if (instance == null)
            instance = new Helper();
        return instance;
    }

    public HashMap<String, Bitmap> imgPreviews = new HashMap<>();
}
