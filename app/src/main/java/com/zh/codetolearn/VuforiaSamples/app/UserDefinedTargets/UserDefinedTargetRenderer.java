/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.zh.codetolearn.VuforiaSamples.app.UserDefinedTargets;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.vuforia.Device;
import com.vuforia.Matrix44F;
import com.vuforia.Renderer;
import com.vuforia.State;
import com.vuforia.Tool;
import com.vuforia.TrackableResult;
import com.vuforia.Vuforia;
import com.zh.codetolearn.ModelLoader;
import com.zh.codetolearn.SampleApplication.SampleAppRenderer;
import com.zh.codetolearn.SampleApplication.SampleAppRendererControl;
import com.zh.codetolearn.SampleApplication.SampleApplicationSession;
import com.zh.codetolearn.SampleApplication.utils.CubeShaders;
import com.zh.codetolearn.SampleApplication.utils.MeshObject;
import com.zh.codetolearn.SampleApplication.utils.SampleApplication3DModel;
import com.zh.codetolearn.SampleApplication.utils.SampleUtils;
import com.zh.codetolearn.SampleApplication.utils.Texture;
import com.zh.codetolearn.object.FilterObject;

import model3D.model.Object3D;
import model3D.model.Object3DBuilder;
import model3D.model.Object3DData;
import model3D.util.GLUtil;


// The renderer class for the ImageTargetsBuilder sample. 
public class UserDefinedTargetRenderer implements GLSurfaceView.Renderer, SampleAppRendererControl
{
    private static final String LOGTAG = "UDTRenderer";

    private SampleApplicationSession vuforiaAppSession;
    private SampleAppRenderer mSampleAppRenderer;

    private boolean mIsActive = false;
    private boolean mIsResourceActive = false;

    private Vector<Texture> mTextures;
    private Vector<Texture> mNewTextures;
    private int shaderProgramID;
    private int vertexHandle;
    private int textureCoordHandle;
    private int mvpMatrixHandle;
    private int texSampler2DHandle;

    // Constants:
    static final float kObjectScale = 3.f;
//    private Teapot mTeapot;

    private int fps = 0;
    
    // Reference to main activity
    private UserDefinedTargets mActivity;

    private int mViewWidth = 0, mViewHeight = 0;

    private boolean requestCapture = false;
    private String fileName ;

    private boolean mModelIsLoaded = false;
    private MeshObject mBuildingsModel;

    private boolean needToInitRenderer = false;
    private String modelPath = "";

    ModelLoader mModelLoader;
    Object3DBuilder drawer;

    // The loaded textures
    private Map<byte[], Integer> textures = new HashMap<byte[], Integer>();

    private long time = System.currentTimeMillis();

    public FilterObject target = null;

    public static int currentType = -1;

    public UserDefinedTargetRenderer(UserDefinedTargets activity,
        SampleApplicationSession session)
    {
        mActivity = activity;
        vuforiaAppSession = session;

        // SampleAppRenderer used to encapsulate the use of RenderingPrimitives setting
        // the device mode AR/VR and stereo mode
        mSampleAppRenderer = new SampleAppRenderer(this, mActivity, Device.MODE.MODE_AR, false, 10f, 5000f);


        // This component will draw the actual models using OpenGL
        drawer = new Object3DBuilder();
        mModelLoader = new ModelLoader(mActivity);
        /*mModelLoader.init("models", "stormtrooper.dae", new ModelLoader.ResourceLoaderListener() {
            @Override
            public void onSuccess() {
                mIsResourceActive = true;
            }

            @Override
            public void onFail() {
                mIsResourceActive = false;
            }
        });*/
        mIsResourceActive = true;

    }


    // Called when the surface is created or recreated.
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        Log.d(LOGTAG, "GLRenderer.onSurfaceCreated");

        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        vuforiaAppSession.onSurfaceCreated();

        mSampleAppRenderer.onSurfaceCreated();
    }


    // Called when the surface changed size.
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        Log.d(LOGTAG, "GLRenderer.onSurfaceChanged");

        this.mViewWidth = width;
        this.mViewHeight = height;

        // Call function to update rendering when render surface
        // parameters have changed:
        mActivity.updateRendering();

        // Call Vuforia function to handle render surface size changes:
        vuforiaAppSession.onSurfaceChanged(width, height);

        // RenderingPrimitives to be updated when some rendering change is done
        mSampleAppRenderer.onConfigurationChanged(mIsActive);

        // Call function to initialize rendering:
//        initRendering("ImageTargets/Buildings.txt"); //default
    }


    public void setActive(boolean active)
    {
        mIsActive = active;

        if(mIsActive)
            mSampleAppRenderer.configureVideoBackground();
    }


    // Called to draw the current frame.ij
    @Override
    public void onDrawFrame(GL10 gl)
    {
        if (!mIsActive)
            return;

        // Call our function to render content from SampleAppRenderer class
        mSampleAppRenderer.render();

        if (System.currentTimeMillis() - time >= 1000) {
            android.util.Log.w("TargetRenderer", fps + " fps");
            fps = 0;
            time = System.currentTimeMillis();
        }
        fps++;

        if (requestCapture) {
            int[] frameBuffer = getFrameBuffer(0, 0, mViewWidth, mViewHeight);
            requestCapture = false;
            saveScreenShot(frameBuffer, mViewWidth, mViewHeight);
        }
    }


    float xRotate = 0, yRotate = 0, zRotate = 0;
    public void setRotate (float x, float y, float z) {
        xRotate = x;
        yRotate = y;
        zRotate = z;
    }

    float scaleX = 3, scaleY = 3, scaleZ = 3;
    public void setScale (float x, float y, float z) {
        scaleX = x;
        scaleY = y;
        scaleZ = z;
    }

    // The render function called from SampleAppRendering by using RenderingPrimitives views.
    // The state is owned by SampleAppRenderer which is controlling it's lifecycle.
    // State should not be cached outside this method.
    public void renderFrame(State state, float[] projectionMatrix)
    {
        boolean ok = false;
        do {
            try {
                mSampleAppRenderer.renderVideoBackground();
                ok = true;
            } catch (Exception e) {
                ok = false;
            }
        }
        while (!ok);
        // Renders video background replacing Renderer.DrawVideoBackground()

        if(needToInitRenderer) {

            initRendering(modelPath);
        }

        if(mTextures == null || mBuildingsModel == null || !mIsResourceActive)
        {
            GLES20.glDisable(GLES20.GL_DEPTH_TEST);

            Renderer.getInstance().end();
            return;
        }

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_CULL_FACE);

        // Render the RefFree UI elements depending on the current state
        mActivity.refFreeFrame.render();
        mActivity.refFreeFrame.render();

        mModelLoader.onDrawFrame();
        // Did we find any trackables this frame?
        for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++)
        {
            // Get the trackable:
            TrackableResult trackableResult = state.getTrackableResult(tIdx);
            Matrix44F modelViewMatrix_Vuforia = Tool
                .convertPose2GLMatrix(trackableResult.getPose());
            float[] modelViewMatrix = modelViewMatrix_Vuforia.getData();

            float[] modelViewProjection = new float[16];
            Matrix.rotateM(modelViewMatrix, 0, 90.f, 0.f, 0f, 0.5f);
//            Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, 0);

            if (xRotate != 0)
                Matrix.rotateM(modelViewMatrix, 0, xRotate, 1f, 0f, 0f);
            if (yRotate != 0)
                Matrix.rotateM(modelViewMatrix, 0, yRotate, 0, 1f, 0);
            if (zRotate != 0)
                Matrix.rotateM(modelViewMatrix, 0, zRotate, 0, 0, 1f);

            Matrix.scaleM(modelViewMatrix, 0, scaleX, scaleY,
                scaleZ);


            Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrix, 0);


            // TODO: set matrix here
            mModelProjectionMatrix = projectionMatrix;
            mModelViewMatrix = modelViewMatrix;

//            if (mBuildingsModel != null) {
//                GLES20.glUseProgram(shaderProgramID);
//
//                GLES20.glDisable(GLES20.GL_CULL_FACE);
//                GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT,
//                        false, 0, mBuildingsModel.getVertices());
//                GLES20.glVertexAttribPointer(textureCoordHandle, 2,
//                        GLES20.GL_FLOAT, false, 0, mBuildingsModel.getTexCoords());
//
//                GLES20.glEnableVertexAttribArray(vertexHandle);
//                GLES20.glEnableVertexAttribArray(textureCoordHandle);
//
//                GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
//                        mTextures.get(0).mTextureID[0]);
//                GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false,
//                        modelViewProjection, 0);
//                GLES20.glUniform1i(texSampler2DHandle, 0);
//                GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0,
//                        mBuildingsModel.getNumObjectVertex());
//
//                SampleUtils.checkGLError("Renderer DrawBuildings");
//
//                SampleUtils.checkGLError("UserDefinedTargets renderFrame");
//            }
            drawObjs2(mModelProjectionMatrix);
        }

        GLES20.glDisable(GLES20.GL_DEPTH_TEST);

        Renderer.getInstance().end();

    }

    public void loadModel(Vector<Texture> textures, String modelPath) {
        // Unload texture:
        this.mModelIsLoaded = false;
        this.modelPath = modelPath;
        needToInitRenderer = true;

        mNewTextures = textures;

    }

    public void loadModel(final FilterObject item, final String assetDir, final String assetName, final ModelLoader.ResourceLoaderListener resourceLoaderListener) {
        this.target = item;
        currentType = target.sourceType;
        mModelLoader.clearObjects();
        mModelLoader.init(assetDir, assetName, resourceLoaderListener);
    }

    void drawObjs2(float[] modelViewProjection) {
        if (mModelLoader == null) return;
        List<Object3DData> objects = mModelLoader.getObjects();
        float[] lightPosInEyeSpace = null;

        for (int i=0; i<objects.size(); i++) {
            Object3DData objData = null;
            try {
                objData = objects.get(i);

                Object3D drawerObject = drawer.getDrawer(objData, mModelLoader.isDrawTextures(), mModelLoader.isDrawLighting());
                // Log.d("ModelRenderer","Drawing object using '"+drawerObject.getClass()+"'");

                Integer textureId = textures.get(objData.getTextureData());
                if (textureId == null && objData.getTextureData() != null) {
                    ByteArrayInputStream textureIs = new ByteArrayInputStream(objData.getTextureData());
                    textureId = GLUtil.loadTexture(textureIs);
                    textureIs.close();
                    textures.put(objData.getTextureData(), textureId);
                }

                drawerObject.draw(objData, mModelProjectionMatrix, mModelViewMatrix,
                        textureId != null ? textureId : -1, lightPosInEyeSpace);

            } catch (Exception ex) {
                Log.e("ModelRenderer","There was a problem rendering the object '"+objData.getId()+"':"+ex.getMessage(),ex);
            }
        }
    }

    // Function for initializing the renderer.
    private void initRendering(String model)
    {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f
                : 1.0f);


        shaderProgramID = SampleUtils.createProgramFromShaderSrc(
                CubeShaders.CUBE_MESH_VERTEX_SHADER,
                CubeShaders.CUBE_MESH_FRAGMENT_SHADER);

        vertexHandle = GLES20.glGetAttribLocation(shaderProgramID,
                "vertexPosition");
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramID,
                "vertexTexCoord");
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID,
                "modelViewProjectionMatrix");
        texSampler2DHandle = GLES20.glGetUniformLocation(shaderProgramID,
                "texSampler2D");

        initTexture(model);
    }

    public void initTexture(String model) {
        if(mTextures != null) {
            mTextures.clear();
            mTextures = null;
        }
        mTextures = new Vector<>(mNewTextures);
        mNewTextures.clear();
        mNewTextures = null;
        for (Texture t : mTextures)
        {
            GLES20.glGenTextures(1, t.mTextureID, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t.mTextureID[0]);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
                    t.mWidth, t.mHeight, 0, GLES20.GL_RGBA,
                    GLES20.GL_UNSIGNED_BYTE, t.mData);
        }

        if(!mModelIsLoaded) {
            try {
                mBuildingsModel = new SampleApplication3DModel();
                ((SampleApplication3DModel)mBuildingsModel).loadModel(mActivity.getResources().getAssets(),
                        model);
                        
                mModelIsLoaded = true;
            } catch (Exception e) {
                Log.e(LOGTAG, "Unable to load buildings");
            }
        }
        needToInitRenderer = false;
    }


    public void setTextures(Vector<Texture> textures)
    {
        mTextures = textures;

    }

    public void requestCaptureFrame(String file) {
        requestCapture = true;
        fileName = file;
    }

    private int[] getFrameBuffer(int x, int y, int w, int h) {
        long ct = System.currentTimeMillis();
        int b[] = new int[w * (y + h)];
        IntBuffer ib = IntBuffer.wrap(b);
        ib.position(0);

        GLES20.glReadPixels(x, 0, w, y + h, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, ib);

        Log.d("TIME", "glReadPixels: " + (System.currentTimeMillis() - ct));
        return b;
    }

    public interface ImageSaveCallback {
        void saved(String path);
    }

    public ImageSaveCallback saveCallback;

    private void saveScreenShot(final int b[], final int w, final int h) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bmp = grabPixels(b, w, h, (int) (0.9f *h));
                try {
                    if(TextUtils.isEmpty(fileName)) {
                        fileName = Environment.getExternalStorageDirectory() + "/ZaloAR_" + System.currentTimeMillis() + ".jpg";
                    }

                    Log.d("AR", fileName);

                    File file = new File(fileName);
                    file.createNewFile();

                    FileOutputStream fos = new FileOutputStream(file);
                    bmp.compress(Bitmap.CompressFormat.JPEG, 80, fos);

                    fos.flush();

                    fos.close();

                    if (saveCallback != null) {
                        saveCallback.saved(fileName);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    private Bitmap grabPixels(int b[], int w, int h, int hlimit) {
//        int b[] = getFrameBuffer(x, y, w, h);
        int bt[] = new int[w * h];

        for (int i = 0, k = 0; i < h; i++, k++) {
            for (int j = 0; j < w; j++) {
                int pix = b[i * w + j];
                int pb = (pix >> 16) & 0xff;
                int pr = (pix << 16) & 0x00ff0000;
                int pix1 = (pix & 0xff00ff00) | pr | pb;
                bt[(h - k - 1) * w + j] = pix1;
            }
        }

//        Bitmap sb = Bitmap.createBitmap(bt, w, h, Bitmap.Config.ARGB_8888);
        return Bitmap.createBitmap(bt, w, hlimit, Bitmap.Config.ARGB_8888);
//        return sb;
    }

    public float getNear() {
        return mSampleAppRenderer.getNear();
    }

    public float getFar() {
        return mSampleAppRenderer.getFar();
    }

    private float[] mModelProjectionMatrix = new float[16];
    private float[] mModelViewMatrix = new float[16];

    public float[] getModelProjectionMatrix() {
        return mModelProjectionMatrix;
    }

    public float[] getModelViewMatrix() {
        return mModelViewMatrix;
    }
}
