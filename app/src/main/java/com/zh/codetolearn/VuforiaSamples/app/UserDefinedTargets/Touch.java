package com.zh.codetolearn.VuforiaSamples.app.UserDefinedTargets;

/**
 * Created by Strongboss on 16/12/2017.
 */

public class Touch {
    public interface ITouchHandler {
        // simple declare
    }

    public interface ITouchClient extends ITouchHandler {
        void onTranslate(float x, float y);

        void onScale(float fractor);

        void onRotate(float angle);

        int getWidth();

        int getHeight();

        float getFar();

        float[] getModelProjectionMatrix();

        float[] getModelViewMatrix();
    }

    public interface ISurfaceRenderer extends ITouchHandler {
        void requestRender();
    }
}
