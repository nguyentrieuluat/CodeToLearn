package com.zh.codetolearn.VuforiaSamples.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zh.codetolearn.R;
import com.zh.codetolearn.widget.CameraEditControl;

public class ImagePreviewActivity extends AppCompatActivity {

    public static final String EXTRA_URL = "url";
    CameraEditControl cameraEditControl;
    String previewUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);
        cameraEditControl = (CameraEditControl) findViewById(R.id.edit_control);
        previewUrl = getIntent().getStringExtra(EXTRA_URL);
    }

    @Override
    protected void onStart() {
        super.onStart();
        cameraEditControl.loadPreview(previewUrl);
    }
}
