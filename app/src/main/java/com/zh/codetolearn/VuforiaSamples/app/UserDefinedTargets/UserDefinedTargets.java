/*===============================================================================
Copyright (c) 2016-2017 PTC Inc. All Rights Reserved.


Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.zh.codetolearn.VuforiaSamples.app.UserDefinedTargets;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.vuforia.CameraDevice;
import com.vuforia.DataSet;
import com.vuforia.ImageTargetBuilder;
import com.vuforia.ObjectTracker;
import com.vuforia.State;
import com.vuforia.Trackable;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;
import com.zh.codetolearn.ModelLoader;
import com.zh.codetolearn.R;
import com.zh.codetolearn.SampleApplication.SampleApplicationControl;
import com.zh.codetolearn.SampleApplication.SampleApplicationException;
import com.zh.codetolearn.SampleApplication.SampleApplicationSession;
import com.zh.codetolearn.SampleApplication.utils.LoadingDialogHandler;
import com.zh.codetolearn.SampleApplication.utils.SampleApplicationGLView;
import com.zh.codetolearn.SampleApplication.utils.Texture;
import com.zh.codetolearn.VuforiaSamples.app.ImagePreviewActivity;
import com.zh.codetolearn.VuforiaSamples.ui.SampleAppMenu.SampleAppMenu;
import com.zh.codetolearn.VuforiaSamples.ui.SampleAppMenu.SampleAppMenuGroup;
import com.zh.codetolearn.VuforiaSamples.ui.SampleAppMenu.SampleAppMenuInterface;
import com.zh.codetolearn.adapter.FilterAdapter;
import com.zh.codetolearn.object.FilterObject;
import com.zh.codetolearn.utils.Utils;
import com.zh.codetolearn.widget.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


// The main activity for the UserDefinedTargets sample.
public class UserDefinedTargets extends Activity implements
    SampleApplicationControl, SampleAppMenuInterface, Touch.ITouchClient,
        Touch.ISurfaceRenderer, ModelLoader.ModelLoaderListener, View.OnClickListener
{
    private static final String LOGTAG = "UserDefinedTargets";
    
    private SampleApplicationSession vuforiaAppSession;

    private TouchController mTouchController;
    
    // Our OpenGL view:
    private SampleApplicationGLView mGlView;
    
    // Our renderer:
    private UserDefinedTargetRenderer mRenderer;
    
    // The textures we will use for rendering:
    private Vector<Texture> mTextures;
    
    // View overlays to be displayed in the Augmented View
    private RelativeLayout mUILayout;
    private View mBottomBar;
    private View mCameraButton;
    private View btnChangeTexture;
    private View btn_switch_cam;
    RecyclerView rcvList;
    
    // Alert dialog for displaying SDK errors
    private AlertDialog mDialog;
    
    int targetBuilderCounter = 1;
    
    DataSet dataSetUserDef = null;
    
    private GestureDetector mGestureDetector;
    
    private SampleAppMenu mSampleAppMenu;
    private ArrayList<View> mSettingsAdditionalViews;
    
    private boolean mExtendedTracking = true;

    private LoadingDialogHandler loadingDialogHandler = new LoadingDialogHandler(
        this);
    
    RefFreeFrame refFreeFrame;
    
    // Alert Dialog used to display SDK errors
    private AlertDialog mErrorDialog;
    
    boolean mIsDroidDevice = false;

    final int REQUEST_CODE_PERMISSION = 1001;

    List<FilterObject> filers;
    
    // Called when the activity first starts or needs to be recreated after
    // resuming the application or a configuration change.
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(LOGTAG, "onCreate");
        super.onCreate(savedInstanceState);
        
        vuforiaAppSession = new SampleApplicationSession(this);
        
        vuforiaAppSession
            .initAR(this, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        // Load any sample specific textures:
        mTextures = new Vector<Texture>();
        mTouchController = new TouchController(this);
        mGestureDetector = new GestureDetector(this, new GestureListener());
        
        mIsDroidDevice = android.os.Build.MODEL.toLowerCase().startsWith("droid");

        filers = new ArrayList<FilterObject>();

        int index = 0;
        FilterObject item;

//        item = new FilterObject(++index, "ImageTargets", "Buildings.txt", "https://images.adsttc.com/media/images/5005/b562/28ba/0d07/7900/1504/large_jpg/stringio.jpg", "Buildings.jpeg");
//        filers.add(item);

        item = new FilterObject(4, "models", "ToyPlane.obj", "https://az616578.vo.msecnd.net/files/2016/03/07/635929352622573191-1360068289_plane-in-blue-sky-136397593033103901-150416160347.jpg", "");
        filers.add(item);

        item = new FilterObject(1, "models", "ship.obj", "https://img00.deviantart.net/6d6d/i/2012/119/5/7/vanguard_spaceship_by_gustvoc-d4xy3sg.jpg", "");
        filers.add(item);

        item = new FilterObject(6, "models", "stormtrooper.dae", "https://img.cinemablend.com/cb/8/e/7/e/3/5/8e7e35cf9777f94ab1f5e51bcdec49214e4ffb2c33f4c575ba4bbb5ff425271c.jpg", "");
        filers.add(item);

        item = new FilterObject(7, "models", "cowboy.dae", "http://static.tvtropes.org/pmwiki/pub/images/john_wayne_cowboy_poster.jpg", "");
        filers.add(item);

        item = new FilterObject(8, "models", "spider.dae", "https://pmcvariety.files.wordpress.com/2017/03/spider-man-homecoming.png", "");

//        item = new FilterObject(2, "models", "LibertStatue.obj", "https://thumbs-prod.si-cdn.com/7VdE97k9HFjZIilzDXe5Cq_rQi0=/800x600/filters:no_upscale()/https://public-media.smithsonianmag.com/filer/b1/69/b1697cf1-1c52-400a-9f0d-9ad87297eecc/42-68884062.jpg", "");
//        filers.add(item);

//        item = new FilterObject(++index, "models", "deer.obj", "https://images.adsttc.com/media/images/5005/b562/28ba/0d07/7900/1504/large_jpg/stringio.jpg", "");
//        filers.add(item);

//        item = new FilterObject(3, "models", "donut.obj", "https://img.grouponcdn.com/iam/6gBemym5xh8B54JGwgRG/au-2048x1229/v1/c700x420.jpg","");
//        filers.add(item);

        item = new FilterObject(5, "models", "wolf.obj", "http://www.spiritanimal.info/wp-content/uploads/Wolf-Spirit-Animal-2.jpg", "");
        filers.add(item);


        filers.add(item);

        addOverlayView(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnChangeTexture:
                showModelList();
                break;
            case R.id.btn_switch_cam:

                break;
        }
    }

    private void showModelList() {
        if (rcvList != null) {
            if (rcvList.getVisibility() == View.GONE)
                rcvList.setVisibility(View.VISIBLE);
            else if (rcvList.getVisibility() == View.VISIBLE)
                rcvList.setVisibility(View.GONE);
        }
    }

    // Process Single Tap event to trigger autofocus
    private class GestureListener extends
        GestureDetector.SimpleOnGestureListener
    {
        // Used to set autofocus one second after a manual focus is triggered
        private final Handler autofocusHandler = new Handler();
        
        
        @Override
        public boolean onDown(MotionEvent e)
        {
            return true;
        }
        
        
        @Override
        public boolean onSingleTapUp(MotionEvent e)
        {
            boolean result = CameraDevice.getInstance().setFocusMode(
                    CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO);
            if (!result)
                Log.e("SingleTapUp", "Unable to trigger focus");

            // Generates a Handler to trigger continuous auto-focus
            // after 1 second
            autofocusHandler.postDelayed(new Runnable()
            {
                public void run()
                {
                    final boolean autofocusResult = CameraDevice.getInstance().setFocusMode(
                            CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);

                    if (!autofocusResult)
                        Log.e("SingleTapUp", "Unable to re-enable continuous auto-focus");
                }
            }, 1000L);

            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {

//            if (Utils.checkPermission(UserDefinedTargets.this, Utils.WRITE_STORAGE_PERMISSION)) {
//                final String fullPath =  Environment.getExternalStorageDirectory() + "/test.png";
//                if(mRenderer != null) {
//                    mRenderer.requestCaptureFrame(fullPath);
//                }
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        String msg = String.format("Save as %s", fullPath);
//                        Toast.makeText(getBaseContext(),msg, Toast.LENGTH_SHORT ).show();
//                    }
//                });
//            } else {
//                ActivityCompat.requestPermissions(UserDefinedTargets.this, new String[] {Utils.WRITE_STORAGE_PERMISSION}, REQUEST_CODE_PERMISSION);
//            }

        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return super.onDoubleTap(e);
        }


    }

    // We want to load specific textures from the APK, which we will later use
    // for rendering.
    private void loadTextures()
    {
        mTextures.clear();

    }
    
    
    // Called when the activity will start interacting with the user.
    @Override
    protected void onResume()
    {
        Log.d(LOGTAG, "onResume");
        super.onResume();

        showProgressIndicator(true);
        
        // This is needed for some Droid devices to force portrait
        if (mIsDroidDevice)
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        vuforiaAppSession.onResume();
    }
    
    
    // Called when the system is about to start resuming a previous activity.
    @Override
    protected void onPause()
    {
        Log.d(LOGTAG, "onPause");
        super.onPause();
        
        if (mGlView != null)
        {
            mGlView.setVisibility(View.INVISIBLE);
            mGlView.onPause();
        }
        
        try
        {
            vuforiaAppSession.pauseAR();
        } catch (SampleApplicationException e)
        {
            Log.e(LOGTAG, e.getString());
        }
    }
    
    
    // The final call you receive before your activity is destroyed.
    @Override
    protected void onDestroy()
    {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();
        
        try
        {
            vuforiaAppSession.stopAR();
        } catch (SampleApplicationException e)
        {
            Log.e(LOGTAG, e.getString());
        }
        
        // Unload texture:
        mTextures.clear();
        mTextures = null;
        
        System.gc();
    }
    
    
    // Callback for configuration changes the activity handles itself
    @Override
    public void onConfigurationChanged(Configuration config)
    {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);
        
        vuforiaAppSession.onConfigurationChanged();
        
        // Removes the current layout and inflates a proper layout
        // for the new screen orientation
        
        if (mUILayout != null)
        {
            mUILayout.removeAllViews();
            ((ViewGroup) mUILayout.getParent()).removeView(mUILayout);
            
        }
        
        addOverlayView(false);
    }
    
    
    // Shows error message in a system dialog box
    private void showErrorDialog()
    {
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
        
        mDialog = new AlertDialog.Builder(UserDefinedTargets.this).create();
        DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        };
        
        mDialog.setButton(DialogInterface.BUTTON_POSITIVE,
            getString(R.string.button_OK), clickListener);
        
        mDialog.setTitle(getString(R.string.target_quality_error_title));
        
        String message = getString(R.string.target_quality_error_desc);
        
        // Show dialog box with error message:
        mDialog.setMessage(message);
        mDialog.show();
    }
    
    
    // Shows error message in a system dialog box on the UI thread
    void showErrorDialogInUIThread()
    {
        runOnUiThread(new Runnable()
        {
            public void run()
            {
                showErrorDialog();
            }
        });
    }
    
    
    // Initializes AR application components.
    private void initApplicationAR()
    {
        // Do application initialization
        refFreeFrame = new RefFreeFrame(this, vuforiaAppSession);
        refFreeFrame.init();
        
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();
        
        mGlView = new SampleApplicationGLView(this);
        mGlView.init(translucent, depthSize, stencilSize);
        
        mRenderer = new UserDefinedTargetRenderer(this, vuforiaAppSession);
//        mRenderer.setTextures(mTextures);
        mGlView.setRenderer(mRenderer);
    }

    private FilterAdapter mFilterAdapter;
    
    // Adds the Overlay view to the GLView
    private void addOverlayView(boolean initLayout)
    {
        // Inflates the Overlay Layout to be displayed above the Camera View
        LayoutInflater inflater = LayoutInflater.from(this);
        mUILayout = (RelativeLayout) inflater.inflate(
            R.layout.camera_overlay_udt, null, false);
        
        mUILayout.setVisibility(View.VISIBLE);
        
        // If this is the first time that the application runs then the
        // uiLayout background is set to BLACK color, will be set to
        // transparent once the SDK is initialized and camera ready to draw
        if (initLayout)
        {
            mUILayout.setBackgroundColor(Color.BLACK);
        }
        
        // Adds the inflated layout to the view
        addContentView(mUILayout, new LayoutParams(LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT));
        
        // Gets a reference to the bottom navigation bar
        mBottomBar = mUILayout.findViewById(R.id.bottom_bar);
        
        // Gets a reference to the Camera button
        mCameraButton = mUILayout.findViewById(R.id.camera_button);
        btn_switch_cam = mUILayout.findViewById(R.id.btn_switch_cam);
        btnChangeTexture = mUILayout.findViewById(R.id.btnChangeTexture);
        btn_switch_cam.setOnClickListener(this);
        btnChangeTexture.setOnClickListener(this);

        rcvList = (RecyclerView) mUILayout.findViewById(R.id.rcvModelList);
        rcvList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        mFilterAdapter = new FilterAdapter(this);
        mFilterAdapter.setFilers(filers);
        rcvList.setAdapter(mFilterAdapter);

        ItemClickSupport.addTo(rcvList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (mFilterAdapter == null || mFilterAdapter.getItemCount() <= position || position < 0)
                    return;

                FilterObject item = mFilterAdapter.getItemFilterObject(position);
                if (item != null && !TextUtils.isEmpty(item.getSourcePath()) && !TextUtils.isEmpty(item.getFolderPath())) {
                    if (isUserDefinedTargetsRunning()) {
                        showModelList();
                        // Shows the loading dialog
                        showProgressIndicator(true);

                        // Builds the new target
                        startBuild(item);

                        mRenderer.loadModel(item, item.getFolderPath(), item.getSourcePath(), new ModelLoader.ResourceLoaderListener() {
                            @Override
                            public void onSuccess() {
                                showProgressIndicator(false);
                            }

                            @Override
                            public void onFail() {
                                showProgressIndicator(false);
                            }
                        });
                    }
                }
            }
        });

        ItemClickSupport.addTo(rcvList).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {

                if (mFilterAdapter != null && position >= 0 && position < mFilterAdapter.getItemCount()) {
                    return true;
                }

                return false;
            }
        });
        
        // Gets a reference to the loading dialog container
        loadingDialogHandler.mLoadingDialogContainer = mUILayout
            .findViewById(R.id.loading_layout);



        initializeBuildTargetModeViews();
        
        mUILayout.bringToFront();

//        new Handler(getMainLooper()).post(rx);
    }

    // Button Camera clicked
    public void onCameraClick(View v)
    {
//        if (isUserDefinedTargetsRunning())
//        {
//            // Shows the loading dialog
//            loadingDialogHandler
//                .sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);
//
//            // Builds the new target
//            startBuild("ImageTargets/Buildings.txt");
//        }

        try {
            if (Utils.checkPermission(UserDefinedTargets.this, Utils.WRITE_STORAGE_PERMISSION)) {
//                final String fullPath =  Environment.getExternalStorageDirectory() + "/ZaloAR_" + System.currentTimeMillis() + ".png";
                if(mRenderer != null) {
                    mRenderer.saveCallback = new UserDefinedTargetRenderer.ImageSaveCallback() {
                        @Override
                        public void saved(final String path) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loadingDialogHandler.sendEmptyMessage(LoadingDialogHandler.HIDE_LOADING_DIALOG);
//                                    String msg = String.format("Save as %s", fullPath);
//                                    Toast.makeText(getBaseContext(),msg, Toast.LENGTH_SHORT ).show();

                                    Intent intent = new Intent(UserDefinedTargets.this, ImagePreviewActivity.class);
                                    intent.putExtra("url", path);
                                    startActivity(intent);
                                }
                            });
                        }
                    };
                    mRenderer.requestCaptureFrame(null);
                    loadingDialogHandler.sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);
                }
            } else {
                ActivityCompat.requestPermissions(UserDefinedTargets.this, new String[] {Utils.WRITE_STORAGE_PERMISSION}, REQUEST_CODE_PERMISSION);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    // Creates a texture given the filename
    Texture createTexture(String nName)
    {
        return Texture.loadTextureFromApk(nName, getAssets());
    }
    
    
    // Callback function called when the target creation finished
    void targetCreated()
    {
        // Hides the loading dialog
        loadingDialogHandler
            .sendEmptyMessage(LoadingDialogHandler.HIDE_LOADING_DIALOG);
        
        if (refFreeFrame != null)
        {
            refFreeFrame.reset();
        }
        
    }
    
    
    // Initialize views
    private void initializeBuildTargetModeViews()
    {
        // Shows the bottom bar
        mBottomBar.setVisibility(View.VISIBLE);
        mCameraButton.setVisibility(View.VISIBLE);
    }
    
    
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if (mTouchController.onTouchEvent(event))
            return true;

        // Process the Gestures
        if (mSampleAppMenu != null && mSampleAppMenu.processEvent(event))
            return true;

        return mGestureDetector.onTouchEvent(event);
    }
    
    
    boolean startUserDefinedTargets()
    {
        Log.d(LOGTAG, "startUserDefinedTargets");
        
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) (trackerManager
            .getTracker(ObjectTracker.getClassType()));
        if (objectTracker != null)
        {
            ImageTargetBuilder targetBuilder = objectTracker
                .getImageTargetBuilder();
            
            if (targetBuilder != null)
            {
                // if needed, stop the target builder
                if (targetBuilder.getFrameQuality() != ImageTargetBuilder.FRAME_QUALITY.FRAME_QUALITY_NONE)
                    targetBuilder.stopScan();
                
                objectTracker.stop();
                
                targetBuilder.startScan();
                
            }
        } else
            return false;
        
        return true;
    }
    
    
    boolean isUserDefinedTargetsRunning()
    {
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
            .getTracker(ObjectTracker.getClassType());
        
        if (objectTracker != null)
        {
            ImageTargetBuilder targetBuilder = objectTracker
                .getImageTargetBuilder();
            if (targetBuilder != null)
            {
                Log.e(LOGTAG, "Quality> " + targetBuilder.getFrameQuality());
                return (targetBuilder.getFrameQuality() != ImageTargetBuilder.FRAME_QUALITY.FRAME_QUALITY_NONE) ? true
                    : false;
            }
        }
        
        return false;
    }
    
    
    void startBuild(FilterObject item)
    {
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
            .getTracker(ObjectTracker.getClassType());
        
        if (objectTracker != null)
        {
            ImageTargetBuilder targetBuilder = objectTracker
                .getImageTargetBuilder();
            if (targetBuilder != null)
            {
                String name;
                do
                {
                    name = "UserTarget-" + targetBuilderCounter;
                    Log.d(LOGTAG, "TRYING " + name);
                    targetBuilderCounter++;
                } while (!targetBuilder.build(name, 500.0f));
                
                refFreeFrame.setCreating();
            }
        }

        if(!TextUtils.isEmpty(item.getTexture())) {
            mTextures.clear();
            mTextures.add(Texture.loadTextureFromApk(item.getFolderPath() + "/" + item.getTexture(),
                    getAssets()));
        }
        mRenderer.loadModel(mTextures, item.getFolderPath() + "/" + item.getSourcePath());
    }
    
    
    void updateRendering()
    {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        refFreeFrame.initGL(metrics.widthPixels, metrics.heightPixels);
        this.mWidth = metrics.widthPixels;
        this.mHeight = metrics.heightPixels;
    }
    
    
    @Override
    public boolean doInitTrackers()
    {
        // Indicate if the trackers were initialized correctly
        boolean result = true;
        
        // Initialize the image tracker:
        TrackerManager trackerManager = TrackerManager.getInstance();
        Tracker tracker = trackerManager.initTracker(ObjectTracker
            .getClassType());
        if (tracker == null)
        {
            Log.d(LOGTAG, "Failed to initialize ObjectTracker.");
            result = false;
        } else
        {
            Log.d(LOGTAG, "Successfully initialized ObjectTracker.");
        }
        
        return result;
    }
    
    
    @Override
    public boolean doLoadTrackersData()
    {
        // Get the image tracker:
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
            .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
        {
            Log.d(
                LOGTAG,
                "Failed to load tracking data set because the ObjectTracker has not been initialized.");
            return false;
        }
        
        // Create the data set:
        dataSetUserDef = objectTracker.createDataSet();
        if (dataSetUserDef == null)
        {
            Log.d(LOGTAG, "Failed to create a new tracking data.");
            return false;
        }
        
        if (!objectTracker.activateDataSet(dataSetUserDef))
        {
            Log.d(LOGTAG, "Failed to activate data set.");
            return false;
        }
        
        Log.d(LOGTAG, "Successfully loaded and activated data set.");
        return true;
    }
    
    
    @Override
    public boolean doStartTrackers()
    {
        // Indicate if the trackers were started correctly
        boolean result = true;
        
        Tracker objectTracker = TrackerManager.getInstance().getTracker(
            ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();
        
        return result;
    }
    
    
    @Override
    public boolean doStopTrackers()
    {
        // Indicate if the trackers were stopped correctly
        boolean result = true;
        
        Tracker objectTracker = TrackerManager.getInstance().getTracker(
            ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();
        
        return result;
    }
    
    
    @Override
    public boolean doUnloadTrackersData()
    {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;
        
        // Get the image tracker:
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
            .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
        {
            result = false;
            Log.d(
                LOGTAG,
                "Failed to destroy the tracking data set because the ObjectTracker has not been initialized.");
        }
        
        if (dataSetUserDef != null)
        {
            if (objectTracker.getActiveDataSet(0) != null
                && !objectTracker.deactivateDataSet(dataSetUserDef))
            {
                Log.d(
                    LOGTAG,
                    "Failed to destroy the tracking data set because the data set could not be deactivated.");
                result = false;
            }
            
            if (!objectTracker.destroyDataSet(dataSetUserDef))
            {
                Log.d(LOGTAG, "Failed to destroy the tracking data set.");
                result = false;
            }
            
            Log.d(LOGTAG, "Successfully destroyed the data set.");
            dataSetUserDef = null;
        }
        
        return result;
    }
    
    
    @Override
    public boolean doDeinitTrackers()
    {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;
        
        if (refFreeFrame != null)
            refFreeFrame.deInit();
        
        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());
        
        return result;
    }

    @Override
    public void onVuforiaResumed()
    {
        if (mGlView != null)
        {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }
    }
    
    @Override
    public void onInitARDone(SampleApplicationException exception)
    {
        
        if (exception == null)
        {
            initApplicationAR();

            mRenderer.setActive(true);

            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            addContentView(mGlView, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
            
            // Sets the UILayout to be drawn in front of the camera
            mUILayout.bringToFront();
            
            // Hides the Loading Dialog
            loadingDialogHandler
                .sendEmptyMessage(LoadingDialogHandler.HIDE_LOADING_DIALOG);
            
            // Sets the layout background to transparent
            mUILayout.setBackgroundColor(Color.TRANSPARENT);
            
            vuforiaAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT);
            
            setSampleAppMenuAdditionalViews();
            mSampleAppMenu = new SampleAppMenu(this, this,
                "User Defined Targets", mGlView, mUILayout,
                mSettingsAdditionalViews);
            setSampleAppMenuSettings();
            
        } else
        {
            Log.e(LOGTAG, exception.getString());
            showInitializationErrorMessage(exception.getString());
        }
    }


    @Override
    public void onVuforiaStarted()
    {
        startUserDefinedTargets();
        
        // Set camera focus mode
        if(!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO))
        {
            // If continuous autofocus mode fails, attempt to set to a different mode
            if(!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO))
            {
                CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);
            }
        }

        showProgressIndicator(false);
    }

    public void showProgressIndicator(boolean show)
    {
        if (loadingDialogHandler != null)
        {
            if (show)
            {
                loadingDialogHandler
                        .sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);
            }
            else
            {
                loadingDialogHandler
                        .sendEmptyMessage(LoadingDialogHandler.HIDE_LOADING_DIALOG);
            }
        }
    }

    // Shows initialization error messages as System dialogs
    public void showInitializationErrorMessage(String message)
    {
        final String errorMessage = message;
        runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (mErrorDialog != null)
                {
                    mErrorDialog.dismiss();
                }
                
                // Generates an Alert Dialog to show the error message
                AlertDialog.Builder builder = new AlertDialog.Builder(
                    UserDefinedTargets.this);
                builder
                    .setMessage(errorMessage)
                    .setTitle(getString(R.string.INIT_ERROR))
                    .setCancelable(false)
                    .setIcon(0)
                    .setPositiveButton(getString(R.string.button_OK),
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                finish();
                            }
                        });
                
                mErrorDialog = builder.create();
                mErrorDialog.show();
            }
        });
    }
    
    
    @Override
    public void onVuforiaUpdate(State state)
    {
        TrackerManager trackerManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) trackerManager
            .getTracker(ObjectTracker.getClassType());
        
        if (refFreeFrame.hasNewTrackableSource())
        {
            Log.d(LOGTAG,
                "Attempting to transfer the trackable source to the dataset");
            
            // Deactivate current dataset
            objectTracker.deactivateDataSet(objectTracker.getActiveDataSet(0));
            
            // Clear the oldest target if the dataset is full or the dataset
            // already contains five user-defined targets.
            if (dataSetUserDef.hasReachedTrackableLimit()
                || dataSetUserDef.getNumTrackables() >= 5)
                dataSetUserDef.destroy(dataSetUserDef.getTrackable(0));
            
            if (mExtendedTracking && dataSetUserDef.getNumTrackables() > 0)
            {
                // We need to stop the extended tracking for the previous target
                // so we can enable it for the new one
                int previousCreatedTrackableIndex = 
                    dataSetUserDef.getNumTrackables() - 1;
                
                objectTracker.resetExtendedTracking();
                dataSetUserDef.getTrackable(previousCreatedTrackableIndex)
                    .stopExtendedTracking();
            }
            
            // Add new trackable source
            Trackable trackable = dataSetUserDef
                .createTrackable(refFreeFrame.getNewTrackableSource());
            
            // Reactivate current dataset
            objectTracker.activateDataSet(dataSetUserDef);
            
            if (mExtendedTracking)
            {
                trackable.startExtendedTracking();
            }
            
        }
    }
    
    final public static int CMD_BACK = -1;
    final public static int CMD_EXTENDED_TRACKING = 1;
    
    // This method sets the additional views to be moved along with the GLView
    private void setSampleAppMenuAdditionalViews()
    {
        mSettingsAdditionalViews = new ArrayList<View>();
        mSettingsAdditionalViews.add(mBottomBar);
    }
    
    
    // This method sets the menu's settings
    private void setSampleAppMenuSettings()
    {
        SampleAppMenuGroup group;
        
        group = mSampleAppMenu.addGroup("", false);
        group.addTextItem(getString(R.string.menu_back), -1);
        
        group = mSampleAppMenu.addGroup("", true);
        group.addSelectionItem(getString(R.string.menu_extended_tracking),
            CMD_EXTENDED_TRACKING, false);
        
        mSampleAppMenu.attachMenu();
    }
    
    
    @Override
    public boolean menuProcess(int command)
    {
        boolean result = true;
        
        switch (command)
        {
            case CMD_BACK:
                finish();
                break;
            
            case CMD_EXTENDED_TRACKING:
                if (dataSetUserDef.getNumTrackables() > 0)
                {
                    int lastTrackableCreatedIndex = 
                        dataSetUserDef.getNumTrackables() - 1;
                    
                    Trackable trackable = dataSetUserDef
                        .getTrackable(lastTrackableCreatedIndex);
                    
                    if (!mExtendedTracking)
                    {
                        if (!trackable.startExtendedTracking())
                        {
                            Log.e(LOGTAG,
                                "Failed to start extended tracking target");
                            result = false;
                        } else
                        {
                            Log.d(LOGTAG,
                                "Successfully started extended tracking target");
                        }
                    } else
                    {
                        if (!trackable.stopExtendedTracking())
                        {
                            Log.e(LOGTAG,
                                "Failed to stop extended tracking target");
                            result = false;
                        } else
                        {
                            Log.d(LOGTAG,
                                "Successfully stopped extended tracking target");
                        }
                    }
                }
                
                if (result)
                    mExtendedTracking = !mExtendedTracking;
                
                break;
            
        }
        
        return result;
    }


    /*
    * GESTURE HANDLE HERE
    * */

    private final float DEFAULT_SCALE = 3f;
    private final float SCALE_DISTANCE = 70;
    private float scaleBase = DEFAULT_SCALE;

    @Override
    public void onScale(float fractor) {
        scaleBase += fractor;
        float fractorChanged = scaleBase / SCALE_DISTANCE;
        if (fractorChanged < DEFAULT_SCALE) {
            scaleBase = 3 * SCALE_DISTANCE;
            return;
        }
        if (mRenderer != null) {
            mRenderer.setScale(fractorChanged, fractorChanged, fractorChanged);
        }
    }

    private float rx = 0;
    private float ry = 0;
    private float rz = 0;
    private final float ROTATE_DISTANCE = 30;


    private float lastY = 0;
    private float lastZ = 0;

    @Override
    public void onTranslate(float x, float y) {
//        Log.w("TAGGGG1", "x = " + x + "y = " + y);

        float ax = Math.abs(x);
        float ay = Math.abs(y);

        rx -= y;
        ry -= x;

        float xx = rx * ROTATE_DISTANCE;
        float yy = ry * ROTATE_DISTANCE;


        if (mRenderer != null) {
            mRenderer.setRotate(xx, yy, 0);
        }

//        float zz = rz * ROTATE_DISTANCE;

//        if (ax <= ay) {
////            lastZ -= Math.sqrt(x * x + y * y) * ROTATE_DISTANCE;
//            ry += 2 * y;
//            yy = ry * ROTATE_DISTANCE;
//            if (mRenderer != null) {
//                mRenderer.setRotate(xx, yy, lastZ);
//            }
//            lastY = yy;
//        } else {
////            lastY -= Math.sqrt(x * x + y * y);
//            if (mRenderer != null) {
//                mRenderer.setRotate(xx, lastY, yy);
//            }
//            lastZ = yy;
//        }
//        mRenderer.setRotate(xx, yy, 0);
    }

    @Override
    public void onRotate(float angle) {
//        Log.w("AAAAA", "rotat a= " + angle);

    }

    @Override
    public void requestRender() {
        if (mGlView != null) {
            mGlView.requestRender();
        }
    }

    private int mWidth = 0;
    private int mHeight = 0;

    @Override
    public int getWidth() {
        return mWidth;
    }

    @Override
    public int getHeight() {
        return mHeight;
    }

    @Override
    public float getFar() {
        if (mRenderer != null) {
            return mRenderer.getFar();
        }
        return 0;

    }

    @Override
    public float[] getModelProjectionMatrix() {
        if (mRenderer != null) {
            return mRenderer.getModelProjectionMatrix();
        }
        return new float[16];
    }

    @Override
    public float[] getModelViewMatrix() {
        if (mRenderer != null) {
            return mRenderer.getModelViewMatrix();
        }
        return new float[16];
    }
}
