package com.zh.codetolearn;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import model3D.animation.Animator;
import model3D.model.Object3DBuilder;
import model3D.model.Object3DData;
import model3D.util.android.Handler;

/**
 * Created by luatnt on 12/16/17.
 */

public class ModelLoader {
    /**
     * List of data objects containing info for building the opengl objects
     */
    private List<Object3DData> objects = new ArrayList<Object3DData>();

    private Activity mActivity;
    GLSurfaceView.Renderer mRenderer;

    /**
     * Whether to draw objects as wireframes
     */
    private boolean drawWireframe = false;
    /**
     * Whether to draw using points
     */
    private boolean drawingPoints = false;
    /**
     * Whether to draw bounding boxes around objects
     */
    private boolean drawBoundingBox = false;
    /**
     * Whether to draw face normals. Normally used to debug models
     */
    private boolean drawNormals = false;
    /**
     * Whether to draw using textures
     */
    private boolean drawTextures = true;
    /**
     * Light toggle feature: we have 3 states: no light, light, light + rotation
     */
    private boolean rotatingLight = false;
    /**
     * Light toggle feature: whether to draw using lights
     */
    private boolean drawLighting = false;

    /**
     * Object selected by the user
     */
    private Object3DData selectedObject = null;

    public ModelLoader(Activity activity) {
        this.mActivity = activity;
    }

    public void init(final String assetDir, final String assetName, final ResourceLoaderListener resourceLoaderListener) {

        // Load object

        // Initialize assets url handler
        Handler.assets = mActivity.getAssets();
        // Handler.classLoader = mActivity.getClassLoader(); (optional)
        // Handler.androidResources = mActivity.getResources(); (optional)

        // Create asset url
        URL url = null;
        try {
//            url = new URL("android://com.zh.codetolearn/assets/" + assetDir + File.separator + assetName);
            url = new URL("file:///android_asset/" + assetDir + File.separator + assetName);
        } catch (MalformedURLException e) {
            Log.e("SceneLoader", e.getMessage(), e);
//            throw new RuntimeException(e);
        }

        Object3DBuilder.loadV6AsyncParallel(mActivity, url, null, assetDir,
                assetName, new Object3DBuilder.Callback() {

                    long startTime = SystemClock.uptimeMillis();

                    @Override
                    public void onBuildComplete(List<Object3DData> datas) {
                        for (Object3DData data : datas) {
                            loadTexture(data, null, assetDir);
                        }
                        final String elapsed = (SystemClock.uptimeMillis() - startTime) / 1000 + " secs";
                        makeToastText("Load complete (" + elapsed + ")", Toast.LENGTH_LONG);

                        if( resourceLoaderListener != null) {
                            resourceLoaderListener.onSuccess();
                        }
                    }

                    @Override
                    public void onLoadComplete(List<Object3DData> datas) {
                        for (Object3DData data : datas) {
                            addObject(data);
                        }
                    }

                    @Override
                    public void onLoadError(Exception ex) {
                        if( resourceLoaderListener != null) {
                            resourceLoaderListener.onFail();
                        }
                        Log.e("SceneLoader", ex.getMessage(), ex);
                        Toast.makeText(mActivity.getApplicationContext(),
                                "There was a problem building the model: " + ex.getMessage(), Toast.LENGTH_LONG)
                                .show();
                    }
                });
    }

    public void loadTexture(Object3DData data, File file, String parentAssetsDir) {
        if (data.getTextureData() == null && data.getTextureFile() != null) {
            try {
                Log.i("SceneLoader", "Loading texture '" + data.getTextureFile() + "'...");
                InputStream stream = null;
                if (file != null) {
                    File textureFile = new File(file.getParent(), data.getTextureFile());
                    stream = new FileInputStream(textureFile);
                } else {
                    stream = mActivity.getAssets().open(parentAssetsDir + "/" + data.getTextureFile());
                }
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                IOUtils.copy(stream, bos);
                stream.close();

                data.setTextureData(bos.toByteArray());
            } catch (IOException ex) {
                makeToastText("Problem loading texture " + data.getTextureFile(), Toast.LENGTH_SHORT);
            }
        }
    }

    public void loadTexture(Object3DData obj, URL path) {
        if (obj == null && objects.size() != 1) {
            makeToastText("Unavailable", Toast.LENGTH_SHORT);
            return;
        }

        try {
            InputStream is = path.openStream();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            IOUtils.copy(is, bos);
            is.close();

            obj = obj != null ? obj : objects.get(0);
            obj.setTextureData(bos.toByteArray());
        } catch (IOException ex) {
            makeToastText("Problem loading texture: " + ex.getMessage(), Toast.LENGTH_SHORT);
        }
    }

    private void makeToastText(final String text, final int toastDuration) {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(mActivity.getApplicationContext(), text, toastDuration).show();
            }
        });
    }

    protected synchronized void addObject(Object3DData obj) {
        List<Object3DData> newList = new ArrayList<Object3DData>(objects);
        newList.add(obj);
        this.objects = newList;
        requestRender();
    }

    public synchronized List<Object3DData> getObjects() {
        return objects;
    }

    public synchronized void clearObjects() {
        objects.clear();
    }

    private void requestRender() {
        if (mActivity instanceof ModelLoaderListener)
            ((ModelLoaderListener) mActivity).requestRender();
    }

    public boolean isDrawWireframe() {
        return this.drawWireframe;
    }

    public boolean isDrawPoints() {
        return this.drawingPoints;
    }

    public boolean isDrawBoundingBox() {
        return drawBoundingBox;
    }

    public boolean isDrawNormals() {
        return drawNormals;
    }

    public boolean isDrawTextures() {
        return drawTextures;
    }

    public boolean isDrawLighting() {
        return drawLighting;
    }

    public Object3DData getSelectedObject() {
        return selectedObject;
    }

    public void setSelectedObject(Object3DData selectedObject) {
        this.selectedObject = selectedObject;
    }

    /**
     * Animator
     */
    private Animator animator = new Animator();
    /**
     * Hook for animating the objects before the rendering
     */
    public void onDrawFrame(){

        if (objects.isEmpty()) return;

        for (Object3DData obj : objects) {
            animator.update(obj);
        }
    }

    public interface ModelLoaderListener {
        void requestRender();
    }

    public interface ResourceLoaderListener {
        void onSuccess();
        void onFail();
    }
}
