package com.zh.codetolearn.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.zh.codetolearn.R;
import com.zh.codetolearn.object.FilterObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luatnt on 12/16/17.
 */

public class FilterPicker {

    Context mContext;
    RecyclerView mRecyclerView;
    FilterAdapter mFilterAdapter;
    FilterPickerListener mFilterPickerListener;

    public FilterPicker(Context context, RecyclerView recyclerView, FilterPickerListener filterPickerListener) {
        this.mContext = context;
        this.mRecyclerView = recyclerView;
        this.mFilterPickerListener = filterPickerListener;
        this.mFilterAdapter = new FilterAdapter(this.mFilterPickerListener);
    }

    public void setDataList(List<FilterObject> filterObjects) {
        if (mFilterAdapter != null) {
            mFilterAdapter.setDataList(filterObjects);
        }
    }

    static class FilterAdapter extends RecyclerView.Adapter<FilterHolder> {
        FilterPickerListener mFilterPickerListener;

        public FilterAdapter(FilterPickerListener filterPickerListener) {
            this.mFilterPickerListener = filterPickerListener;
        }

        List<FilterObject> mDataList = new ArrayList<>();

        @Override
        public FilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_item_view, parent, false);
            return new FilterHolder(itemView);
        }

        @Override
        public void onBindViewHolder(FilterHolder holder, int position) {
            try {
                final FilterObject filterObject = mDataList.get(position);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mFilterPickerListener != null) {
                            mFilterPickerListener.onFilterSelected(filterObject);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return mDataList.size();
        }

        public void setDataList(List<FilterObject> filterObjects) {
            mDataList.clear();
            if (filterObjects != null) {
                mDataList.addAll(filterObjects);
            }
        }
    }

    static class FilterHolder extends RecyclerView.ViewHolder {
        ImageView mImvThumb;

        public FilterHolder(View itemView) {
            super(itemView);
            mImvThumb = (ImageView) itemView.findViewById(R.id.filter_item_imv_thumb);
        }
    }

    public interface FilterPickerListener {
        void onFilterSelected(FilterObject filterObject);
    }
}
