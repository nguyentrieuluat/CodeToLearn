package com.zh.codetolearn.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.zh.codetolearn.R;
import com.zh.codetolearn.utils.Utils;

/**
 * Created by luatnt on 12/16/17.
 */

public class CameraEditControl extends RelativeLayout implements View.OnClickListener {
    View mBottomControlPanel;
    View mBtnShare;
    ImageView mImvPreview;
    String previewUrl;
    private Handler handler = new Handler();

    public CameraEditControl(@NonNull Context context) {
        super(context);
    }

    public CameraEditControl(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraEditControl(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater.from(getContext()).inflate(R.layout.camera_edit_control, this);
        mBottomControlPanel = findViewById(R.id.camera_edit_control_bottom_panel);

        mBtnShare = findViewById(R.id.btn_share);
        mBtnShare.setOnClickListener(this);

        mImvPreview = (ImageView) findViewById(R.id.imv_preview);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_share:
                Utils.shareWithIntent(getContext(), previewUrl);
                break;
        }
    }

    public void loadPreview(final String url) {
        previewUrl = url;
//        Glide.with(getContext())
//                .load(url)
//                .placeholder(R.drawable.img_filter_thumb)
//                .error(R.drawable.img_filter_thumb)
//                .into(mImvPreview);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = BitmapFactory.decodeFile(url);
                final Bitmap scaler = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 3, bitmap.getHeight() / 3, false);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mImvPreview.setImageBitmap(scaler);
                    }
                });
            }
        }).start();
    }
}
