package com.zh.codetolearn.widget;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.zh.codetolearn.R;
import com.zh.codetolearn.object.FilterObject;

/**
 * Created by luatnt on 12/16/17.
 */

public class CameraPreviewControl extends FrameLayout implements View.OnClickListener, FilterPicker.FilterPickerListener {
    View mTopControlPanel;
    View mBottomControlPanel;
    View mBtnCapture;
    ImageView mBtnSwitchCam;
    ImageView mBtnFlash;
    ImageView mBtnFilter;
    FilterPicker mFilterPicker;
    public RecyclerView mFilterRecyclerView;

    public CameraPreviewControl(@NonNull Context context) {
        super(context);
    }

    public CameraPreviewControl(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraPreviewControl(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater.from(getContext()).inflate(R.layout.camera_preview_control, this);
        mTopControlPanel = findViewById(R.id.camera_preview_control_top_panel);
        mBottomControlPanel = findViewById(R.id.camera_preview_control_bottom_panel);

        mBtnSwitchCam = (ImageView) findViewById(R.id.btn_switch_cam);
        mBtnFlash = (ImageView) findViewById(R.id.btn_flash);
        mBtnFilter = (ImageView) findViewById(R.id.btn_filter);
        mBtnCapture = findViewById(R.id.btn_capture);

        mBtnSwitchCam.setOnClickListener(this);
        mBtnFlash.setOnClickListener(this);
        mBtnFilter.setOnClickListener(this);
        mBtnCapture.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_switch_cam:
                switchCamera();
                break;
            case R.id.btn_flash:
                switchFlash();
                break;
            case R.id.btn_filter:
                showFilterPicker();
                break;
            case R.id.btn_capture:
                capture();
                break;
        }
    }

    void initFilterPicker() {
        if (mFilterPicker == null) {
            mFilterRecyclerView = new RecyclerView(getContext());
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity = Gravity.BOTTOM;
            addView(mFilterRecyclerView, layoutParams);
            mFilterPicker = new FilterPicker(getContext(), mFilterRecyclerView, this);
        }
    }

    void showFilterPicker() {
        initFilterPicker();
        mFilterPicker.mRecyclerView.setVisibility(VISIBLE);
        mBottomControlPanel.setVisibility(GONE);
        mBtnCapture.setVisibility(GONE);
    }

    void hideFilterPicker() {
        if (mFilterPicker == null) return;
        mFilterPicker.mRecyclerView.setVisibility(GONE);
        mBottomControlPanel.setVisibility(VISIBLE);
        mBtnCapture.setVisibility(VISIBLE);
    }

    @Override
    public void onFilterSelected(FilterObject filterObject) {

    }

    void capture() {
        // TODO: 12/16/17 Capture photo
    }

    void switchCamera() {
        // TODO: 12/16/17 Switch camera
//        mBtnSwitchCam.setImageResource();
    }

    void switchFlash() {
        // TODO: 12/16/17 Switch flash
//        mBtnFlash.setImageResource();
    }

    public boolean onKeyUp(KeyEvent keyEvent) {
        if (keyEvent == null) return false;
        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (mFilterPicker != null && mFilterPicker.mRecyclerView.getVisibility() == VISIBLE) {
                hideFilterPicker();
                return true;
            }
        }
        return false;
    }
}
