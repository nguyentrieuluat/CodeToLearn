package com.zh.codetolearn.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.zh.codetolearn.R;
import com.zh.codetolearn.object.FilterObject;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by Strongboss on 16/12/2017.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.FilterViewHolder> {
    private List<FilterObject> filers;
    private Context localContext;

    public FilterAdapter(Context context) {
        localContext = context;
        filers = new ArrayList<>();
    }

    public FilterAdapter(List<FilterObject> filers) {
        this.filers = filers;
    }

    public void setFilers(List<FilterObject> filers) {
        if (filers == null)
            return;
        this.filers = new ArrayList<>(filers);
        notifyDataSetChanged();
    }

    public FilterObject getItemFilterObject(int pos) {
        if (pos >= 0 && filers != null && pos < filers.size())
            return filers.get(pos);

        return null;
    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.filter_item_layout, parent, false);
        return new FilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        FilterObject item = filers.get(position);
        if(item != null)
        {
            holder.imgThumb.setImageResource(R.drawable.img_filter_thumb);
//            Glide.with(localContext)
//                    .load(item.getImagePreview())
//                    .centerCrop()
//                    .into(holder.imgThumb);

            int radius = 20; // corner radius, higher value = more rounded
            int margin = 7; // crop margin, set to 0 for corners with no crop
            Glide.with(localContext)
                    .load(item.getImagePreview())
                    .centerCrop()
                    .placeholder(R.drawable.img_filter_thumb)
                    .error(R.drawable.img_filter_thumb)
                    .bitmapTransform(new RoundedCornersTransformation(localContext, radius, margin))
                    .into(holder.imgThumb);
        }
    }

    @Override
    public int getItemCount() {
        if (filers == null)
            filers = new ArrayList<>();
        return filers.size();
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgThumb;

        public FilterViewHolder(View itemView) {
            super(itemView);
            imgThumb = (ImageView) itemView.findViewById(R.id.imgThumb);
        }
    }
}
