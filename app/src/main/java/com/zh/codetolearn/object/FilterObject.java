package com.zh.codetolearn.object;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.zh.codetolearn.SampleApplication.utils.MeshObject;
import com.zh.codetolearn.helper.Helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luatnt on 12/16/17.
 */

public class FilterObject {
    public final static int MESH = 0;
    public final static int EXTERNAL_FILE = 1;

    private final String meshPack = "com.zh.codetolearn.SampleApplication.utils";

    public int sourceType;
    public String sourcePath;

    private String folderPath;
    private String imagePreview;

    String mTextture;

    public FilterObject(int sourceType, String folderPath, String sourcePath, String imagePreview, String texture) {
        this.sourceType = sourceType;
        this.sourcePath = sourcePath;
        this.imagePreview = imagePreview;
        this.folderPath = folderPath;
        this.mTextture = texture;
    }

    public MeshObject getMesh() {
        if (sourceType == MESH) {
            try {
                Class<?> objClass = Class.forName(sourcePath);
                if (objClass != null) {
                    return (MeshObject) objClass.cast(MeshObject.class);
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public Bitmap getBitmapPreview() {
        if (Helper.getInstance().imgPreviews.containsKey(imagePreview)) {
            return Bitmap.createBitmap(Helper.getInstance().imgPreviews.get(imagePreview));
        }
        try {
            return BitmapFactory.decodeFile(imagePreview);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getSourceType() {
        return sourceType;
    }

    public void setSourceType(int sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public String getImagePreview() {
        return imagePreview;
    }

    public void setImagePreview(String imagePreview) {
        this.imagePreview = imagePreview;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public CharSequence getTexture() {
        return mTextture;
    }
}
