package com.zh.codetolearn.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;

import java.io.File;

/**
 * Created by luatnt on 12/16/17.
 */

public class Utils {
    final public static String CAMERA_PERMISSION = Manifest.permission.CAMERA;
    final public static String WRITE_STORAGE_PERMISSION = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static boolean checkPermission(Context context, String permission) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return true;
        int permissionResult = context.checkSelfPermission(permission);
        return permissionResult == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermissions(Activity activity, String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return;
        activity.requestPermissions(permissions, requestCode);
    }

    public static void shareWithIntent(Context activity, String filePath)  {
        Intent intentShareFile = new Intent(Intent.ACTION_SEND);
        File fileWithinMyDir = new File(filePath);

        if(fileWithinMyDir.exists()) {
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse(filePath));
            intentShareFile.setType("image/*");
            intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                    "AR");
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Đã gửi từ AR Project");

            activity.startActivity(Intent.createChooser(intentShareFile, "Share File"));
        }
    }

//    public static void shareBitmapUsingFacebook(Activity activity, Bitmap bitmap) {
//        SharePhoto photo = new SharePhoto.Builder()
//                .setBitmap(bitmap)
//                .build();
//        SharePhotoContent content = new SharePhotoContent.Builder()
//                .addPhoto(photo)
//                .build();
//
//        ShareDialog shareDialog = new ShareDialog(activity);
//        shareDialog.show(content);
//    }
}
